package controllers;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.MainStage;
import others.MyClassTextFieldListener;

import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SettingsController {

    @FXML
    private VBox vBoxSettings;
    @FXML
    private TextField textFieldApiKey;
    @FXML
    private ToggleGroup toggleGroupCity;
    @FXML
    private RadioButton radioButtonCityId;
    @FXML
    private RadioButton radioButtonGeoLocation;
    @FXML
    private TextField textFieldCityId;
    @FXML
    private TextField textFieldGeoLocationLatitude;
    @FXML
    private TextField textFieldGeoLocationLongitude;

    @FXML
    private ToggleGroup toggleGroupTemperature;
    @FXML
    private RadioButton radioButtonCelsius;
    @FXML
    private RadioButton radioButtonKelvin;
    @FXML
    private RadioButton radioButtonFahrenheit;

    @FXML
    private CheckBox checkBoxDescription;
    @FXML
    private ToggleGroup toggleGroupLanguage;
    @FXML
    private RadioButton radioButtonEN;
    @FXML
    private RadioButton radioButtonPL;
    @FXML
    private RadioButton radioButtonDE;
    @FXML
    private CheckBox checkBoxHumidity;
    @FXML
    private CheckBox checkBoxTimeOfSunriseAndSunset;
    @FXML
    private Button buttonSave;

    private String stringTemperatureUnit = "units=metric";
    private final String PATH_FILE = "./src/main/resources/txt/widgetSettings.txt";
    private String stringCityIdOrGeoLocation = "";//"id=3081368";
    private String stringBeginningTextFieldApiKey;
    private String stringBeginningTextFieldCityId;
    private String stringBeginningTextFieldGeoLocationLatitude;
    private String stringBeginningTextFieldGeoLocationLongitude;
    private String stringBeginningSelectedRadioButtonTemperatureUnit;
    private String stringBeginningSelectedRadioButtonLanguage;

    private RadioButton selectedRadioButtonLanguage;
    private boolean booleanBeginningSelectedRadioButtonCityId;
    private boolean booleanBeginningSelectedRadioButtonGeoLocation;
    private double v90;
    private double v180;

    @FXML
    public void initialize() {
        createContextMenu();
        textFieldGeoLocationLatitude.setDisable(true);
        textFieldGeoLocationLongitude.setDisable(true);

        textFieldGeoLocationLatitude.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
        textFieldGeoLocationLongitude.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);

        textFieldGeoLocationLatitude.addEventFilter(KeyEvent.KEY_PRESSED, disableCtrlV);
        textFieldGeoLocationLongitude.addEventFilter(KeyEvent.KEY_PRESSED, disableCtrlV);

//        textFieldGeoLocationLatitude.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                if(newValue.matches("-?((\\d*)|(\\d+\\.\\d*))")) { // -?((\\d*)|(\\d+\\.\\d*))
//                    textFieldGeoLocationLatitude.setText(newValue);
//                } else {
//                    Toolkit.getDefaultToolkit().beep();
//                }
//            }
//        });

//        radioButtonCityId.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
//                if(radioButtonCityId.isSelected()) {
//                    buttonSave.setDisable(false);
//                }
//            }
//        });

        radioButtonCityId.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                buttonSave.setDisable(false);
            }
        });

//        radioButtonGeoLocation.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
//                setV90andV180();
//                if( (v180 <= 180) && (v180 >= -180) && (v90 <= 90) && (v90 >= -90) ) {
//                    buttonSave.setDisable(false);
//                }
//                else {
//                    buttonSave.setDisable(true);
//                }
//            }
//        });

        radioButtonGeoLocation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setV90andV180();
                if( (v180 <= 180) && (v180 >= -180) && (v90 <= 90) && (v90 >= -90) ) {
                    buttonSave.setDisable(false);
                }
                else {
                    buttonSave.setDisable(true);
                }
            }
        });

        textFieldGeoLocationLatitude.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                double v = Double.parseDouble(newValue);
//                v90 = Double.parseDouble(textFieldGeoLocationLatitude.getText());
//                v180 = Double.parseDouble(textFieldGeoLocationLongitude.getText());
                setV90andV180();
                if( (v90 <= 90) && (v90 >= -90) ) {
                    textFieldGeoLocationLatitude.setStyle("-fx-background-color:white");
                } else {
                    textFieldGeoLocationLatitude.setStyle("-fx-background-color:#ff6666");
                }
                if( (v180 <= 180) && (v180 >= -180) && (v90 <= 90) && (v90 >= -90) ) {
                    buttonSave.setDisable(false);
                } else {
                    buttonSave.setDisable(true);
                }
//                if(radioButtonCityId.isSelected()) {
//                    buttonSave.setDisable(false);
//                }
            }
        });

        textFieldGeoLocationLongitude.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                setV90andV180();
//                v90 = Double.parseDouble(textFieldGeoLocationLatitude.getText());
//                v180 = Double.parseDouble(textFieldGeoLocationLongitude.getText());
                if( (v180 <= 180) && (v180 >= -180) ) {
                    textFieldGeoLocationLongitude.setStyle("-fx-background-color:white");
                } else {
                    textFieldGeoLocationLongitude.setStyle("-fx-background-color:#ff6666");
                }
                if( (v180 <= 180) && (v180 >= -180) && (v90 <= 90) && (v90 >= -90)  ) {
                    buttonSave.setDisable(false);
                } else {
                    buttonSave.setDisable(true);
                }
//                if(radioButtonCityId.isSelected()) {
//                    buttonSave.setDisable(false);
//                }
            }
        });


//        String regexGeoLocation = "-?((\\d*)|(\\d+(\\.|,)\\d*))";
        String regexGeoLocationLongitude = "-?((\\d{0,3})|(\\d{1,3}(\\.|,)\\d{0,4}))";
        String regexGeoLocationLatitude = "-?((\\d{0,2})|(\\d{1,2}(\\.|,)\\d{0,4}))";

        textFieldGeoLocationLongitude.textProperty().addListener(new MyClassTextFieldListener(textFieldGeoLocationLongitude, regexGeoLocationLongitude));
        textFieldGeoLocationLatitude.textProperty().addListener(new MyClassTextFieldListener(textFieldGeoLocationLatitude, regexGeoLocationLatitude));

        File myFile = new File(PATH_FILE);
        try {
            FileReader myFileReader = new FileReader(myFile);
            BufferedReader myBufferedReader = new BufferedReader(myFileReader);

            String textFromFile;

            while((textFromFile = myBufferedReader.readLine()) != null) {
//                System.out.println(textFromFile);
                String[] stringTab = textFromFile.split("/");
//                mapForFile.put(stringTab[0], stringTab[1]);
                if("apiKey".equals(stringTab[0])) {
                    textFieldApiKey.setText(stringTab[1].substring(6));
                } else if("cityIdOrGeoLocation".equals(stringTab[0])) {
                    char tempchar = stringTab[1].charAt(0);
                    if('i' == tempchar ) {
                        textFieldCityId.setText(stringTab[1].substring(3));
                    } else if('l' == tempchar) {
                        radioButtonCityId.setSelected(false);
                        textFieldCityId.setDisable(true);
                        radioButtonGeoLocation.setSelected(true);
                        textFieldGeoLocationLongitude.setDisable(false);
                        textFieldGeoLocationLatitude.setDisable(false);
                        String[] str = stringTab[1].split("&lon=");
                        textFieldGeoLocationLatitude.setText(str[0].substring(4));
                        if(str.length != 1 ) {
                            textFieldGeoLocationLongitude.setText(str[1]); // .substring(0)
                        }
                    }
                } else if("cityId".equals(stringTab[0])) {
                    textFieldCityId.setText(stringTab[1].substring(3));;
                } else if("geoLocationLatitude".equals(stringTab[0])) {
                    textFieldGeoLocationLatitude.setText(stringTab[1].substring(4));
                } else if("geoLocationLongitude".equals(stringTab[0])) {
                    textFieldGeoLocationLongitude.setText(stringTab[1].substring(4));
                } else if("temperatureUnit".equals(stringTab[0]) ) {
                    String tempString = stringTab[1].substring(6);
                    if("metric".equals(tempString)) {
                        toggleGroupTemperature.selectToggle(radioButtonCelsius);
                        stringTemperatureUnit = "units=metric";
                    } else if("standard".equals(tempString)) { // "".equals(tempString)  ArrayIndexOutOfBoundsException: 1
                        toggleGroupTemperature.selectToggle(radioButtonKelvin);
                        stringTemperatureUnit = "units=standard";
                    } else if("imperial".equals(tempString)) {
                        toggleGroupTemperature.selectToggle(radioButtonFahrenheit);
                        stringTemperatureUnit = "units=imperial";
                    }
                } else if("checkBoxDescription".equals(stringTab[0])) {
                    if("true".equals(stringTab[1])) {
                        setWhichToggleGroupDisable(toggleGroupLanguage, false);
                    } else {
                        setWhichToggleGroupDisable(toggleGroupLanguage, true);
                    }
                } else if("lang".equals(stringTab[0])) {
//                    RadioButton selectedRadioButton = (RadioButton) toggleGroupLanguage.selectToggle(toggleGroupLanguage.getToggles());
                    if("en".equals(stringTab[1].substring(5))) {
                        toggleGroupLanguage.selectToggle(radioButtonEN);
                    } else if("pl".equals(stringTab[1].substring(5))) {
                        toggleGroupLanguage.selectToggle(radioButtonPL);
                    } else if("de".equals(stringTab[1].substring(5))) {
                        toggleGroupLanguage.selectToggle(radioButtonDE);
                    }
                }
            }

            myBufferedReader.close();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
            System.out.println("\n\nFile not found\n");
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("\n\nreadLine() Exception\n");
        }

        createSelectedProperties();

        if(MainStage.mainController.getLabelDescription().isVisible()) {
            checkBoxDescription.setSelected(true);
        } else {
            checkBoxDescription.setSelected(false);
        }

        if(MainStage.mainController.getLabelHumidity().isVisible()) {
            checkBoxHumidity.setSelected(true);
        } else {
            checkBoxHumidity.setSelected(false);
        }

        if(MainStage.mainController.getLabelSunrise().isVisible()) {
            checkBoxTimeOfSunriseAndSunset.setSelected(true);
        } else {
            checkBoxTimeOfSunriseAndSunset.setSelected(false);
        }

        stringBeginningTextFieldApiKey = textFieldApiKey.getText();
        stringBeginningTextFieldCityId = textFieldCityId.getText();
        stringBeginningTextFieldGeoLocationLatitude = textFieldGeoLocationLatitude.getText();
        stringBeginningTextFieldGeoLocationLongitude = textFieldGeoLocationLongitude.getText();
        selectedRadioButtonLanguage = (RadioButton) toggleGroupLanguage.getSelectedToggle();
        stringBeginningSelectedRadioButtonLanguage = ((RadioButton) toggleGroupLanguage.getSelectedToggle()).getText();
//        selectedRadioButtonTe = (RadioButton) toggleGroupTemperature.getSelectedToggle();
        stringBeginningSelectedRadioButtonTemperatureUnit = ((RadioButton) toggleGroupTemperature.getSelectedToggle()).getText();
        if ("Celsius".equals(stringBeginningSelectedRadioButtonTemperatureUnit)) {
            stringBeginningSelectedRadioButtonTemperatureUnit = "units=metric";
        } else if ("Kelvin".equals(stringBeginningSelectedRadioButtonTemperatureUnit)) {
            stringBeginningSelectedRadioButtonTemperatureUnit = "units=standard";
        } else if ("Fahrenheit".equals(stringBeginningSelectedRadioButtonTemperatureUnit)) {
            stringBeginningSelectedRadioButtonTemperatureUnit = "units=imperial";
        }
        booleanBeginningSelectedRadioButtonCityId = radioButtonCityId.isSelected();
        booleanBeginningSelectedRadioButtonGeoLocation = radioButtonGeoLocation.isSelected();
        if(booleanBeginningSelectedRadioButtonCityId) {
            buttonSave.setDisable(false);
        }
    }

    private void setV90andV180() {
        try{
            v90 = Double.parseDouble(textFieldGeoLocationLatitude.getText().replaceAll(",","\\."));
        } catch (NumberFormatException nfe) {
            v90 = 0;
        }
        try{
            v180 = Double.parseDouble(textFieldGeoLocationLongitude.getText().replaceAll(",","\\."));
        } catch (NumberFormatException nfe) {
            v180 = 0;
        }
    }

    public EventHandler<KeyEvent> disableCtrlV = new EventHandler<KeyEvent>() {
        final KeyCombination keyCombCtrlV = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY);
//        final KeyCombination keyCombCtrlDOWN = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN);
        public void handle(KeyEvent ke) {
            if (keyCombCtrlV.match(ke)) {
                Toolkit.getDefaultToolkit().beep();
                ke.consume();
                System.out.println("(#SettingsController) Key Pressed: " + ke.getCode());
            }
        }
    };

    private void setLanguageRadioButtonsDisableOrNotDisable() {
        if(checkBoxDescription.isSelected()) {
            setWhichToggleGroupDisable(toggleGroupLanguage, false);
        } else {
            setWhichToggleGroupDisable(toggleGroupLanguage, true);
        }
    }

    private void setWhichToggleGroupDisable(ToggleGroup toggleGroup, boolean disableTrueFalse) {
        toggleGroup.getToggles().forEach(toggle -> {
            Node node = (Node) toggle ;
            node.setDisable(disableTrueFalse);
        });
    }

    private void createSelectedProperties() {
        toggleGroupCity.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle t1) {
//                RadioButton selectedRadioButton = (RadioButton) toggleGroupCity.getSelectedToggle();
                if(radioButtonCityId.isSelected()) {
                    textFieldCityId.setDisable(false);
                    textFieldGeoLocationLatitude.setDisable(true);
                    textFieldGeoLocationLongitude.setDisable(true);
                } else if(radioButtonGeoLocation.isSelected()) {
                    textFieldCityId.setDisable(true);
                    textFieldGeoLocationLatitude.setDisable(false);
                    textFieldGeoLocationLongitude.setDisable(false);
                }
            }
        });
        toggleGroupTemperature.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle t1) {
                if(radioButtonCelsius.isSelected() && MainController.isInternetConnection()) {
                    stringTemperatureUnit = "units=metric";
                } else if(radioButtonKelvin.isSelected()) {
                    stringTemperatureUnit = "units=standard"; // units=standard
                } else if(radioButtonFahrenheit.isSelected()) {
                    stringTemperatureUnit = "units=imperial";
                }
//                https://www.geeksforgeeks.org/javafx-radiobutton-with-examples/
            }
        });
        checkBoxDescription.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(checkBoxDescription.isSelected()) {
                    MainStage.mainController.getLabelDescription().setVisible(true);
                } else {
                    MainStage.mainController.getLabelDescription().setVisible(false);
                }

                setLanguageRadioButtonsDisableOrNotDisable();
            }
        });
        checkBoxHumidity.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(checkBoxHumidity.isSelected()) {
                    MainStage.mainController.getLabelHumidity().setVisible(true);
                } else {
                    MainStage.mainController.getLabelHumidity().setVisible(false);
                }
            }
        });
        checkBoxTimeOfSunriseAndSunset.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(checkBoxTimeOfSunriseAndSunset.isSelected()) {
                    MainStage.mainController.getLabelSunrise().setVisible(true);
                    MainStage.mainController.getLabelSunset().setVisible(true);
                } else {
                    MainStage.mainController.getLabelSunrise().setVisible(false);
                    MainStage.mainController.getLabelSunset().setVisible(false);
                }
            }
        });
    }

    private void createContextMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem closeWindow = new MenuItem("Close window");
        closeWindow.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                closeWindow();
            }
        });
        contextMenu.getItems().add(closeWindow);
        vBoxSettings.setOnContextMenuRequested(e -> {
            contextMenu.show(vBoxSettings.getScene().getWindow(), e.getScreenX(), e.getScreenY());
        });
    }

    @FXML
    public void getApiKey(ActionEvent event) {
//        try {
//            new ProcessBuilder("x-www-browser", "https://openweathermap.org/guide").start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //
        String strinURL = "https://openweathermap.org/guide";
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Win")) {
            try {
                Desktop.getDesktop().browse(new URL(strinURL).toURI());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else // (osName.startsWith("Linux") || osName.startsWith("Mac")) {
            try {
                new ProcessBuilder("x-www-browser", strinURL).start();
            } catch (IOException e) {
                e.printStackTrace();
        }
    }

    @FXML
    public void cancel(ActionEvent event) {
        closeWindow();
        MainStage.mainController.setVBoxMainNotDisable();
    }

    @FXML
    public void save(ActionEvent event) {
        System.out.println("\n button save \n");
        Platform.runLater(new Runnable() {
            @Override
            public synchronized void run() {
                deleteWhiteSpacesAtBeginningAndEndOf(textFieldApiKey);
                deleteWhiteSpacesAtBeginningAndEndOf(textFieldCityId);
                deleteWhiteSpacesAtBeginningAndEndOf(textFieldGeoLocationLatitude);
                deleteWhiteSpacesAtBeginningAndEndOf(textFieldGeoLocationLongitude);
                deleteDecimalPointAtEndOfStringIfExists(textFieldGeoLocationLatitude);
                deleteDecimalPointAtEndOfStringIfExists(textFieldGeoLocationLongitude);

                if(radioButtonCityId.isSelected() ) {
                    stringCityIdOrGeoLocation = "id=" + textFieldCityId.getText();
                } else if(radioButtonGeoLocation.isSelected() ) {
                    stringCityIdOrGeoLocation = "lat=" + textFieldGeoLocationLatitude.getText()
                            + "&lon=" + textFieldGeoLocationLongitude.getText();
                }

                saveDataToFile();

                RadioButton selectedRadioButtonLanguage = (RadioButton) toggleGroupLanguage.getSelectedToggle();
                RadioButton selectedRadioButtonTemperatureUnit = (RadioButton) toggleGroupTemperature.getSelectedToggle();
                if( !(stringBeginningTextFieldApiKey.equals(textFieldApiKey.getText()) ) ) {
                    MainStage.mainController.updateValues();
                } else if(radioButtonCityId.isSelected() && !booleanBeginningSelectedRadioButtonCityId ) {
                    System.out.println("(#SettingsController) radioButtonCityId.isSelected() && !booleanBeginningSelectedRadioButtonCityId");
                    MainStage.mainController.updateValues();
                } else if(radioButtonCityId.isSelected() && (!(stringBeginningTextFieldCityId.equals(textFieldCityId.getText())))) {
                    System.out.println("(#SettingsController) radioButtonCityId.isSelected()");
                    MainStage.mainController.updateValues();
                } else if(radioButtonGeoLocation.isSelected() && (!booleanBeginningSelectedRadioButtonGeoLocation) ) {
                    System.out.println("(#SettingsController) radioButtonGeoLocation.isSelected() && (!booleanBeginningSelectedRadioButtonGeoLocation)");
                    MainStage.mainController.updateValues();
                } else if(radioButtonGeoLocation.isSelected() && (!(stringBeginningTextFieldGeoLocationLatitude.equals(textFieldGeoLocationLatitude.getText()))) || (!(stringBeginningTextFieldGeoLocationLongitude.equals(textFieldGeoLocationLongitude.getText()))) ) {
                    System.out.println("(#SettingsController) radioButtonGeoLocation.isSelected()");
                    MainStage.mainController.updateValues();
                } else if( !(stringBeginningSelectedRadioButtonTemperatureUnit.equals(selectedRadioButtonTemperatureUnit.getText()))) {
                    System.out.println("(#SettingsController) temperature unit ");
                    MainStage.mainController.updateValues();
                } else if(checkBoxDescription.isSelected()) { // ten warunek musi być na końcu
                    System.out.println("(#SettingsController) checkBoxDescription.isSelected()");
                    if(!(stringBeginningSelectedRadioButtonLanguage.equals(selectedRadioButtonLanguage.getText()))) {
                        MainStage.mainController.updateValues();
                    }
                } else {
                    System.out.println("(#SettingsController) funkcja save else nothing");
                }

                MainStage.mainController.setVBoxMainNotDisable();
            }
        });

        closeWindow();
    }

    private void saveDataToFile() {
        File myFile = new File(PATH_FILE);
        myFile.delete();
        FileWriter fileWriter;

        try {
            fileWriter = new FileWriter(PATH_FILE);
            fileWriter.write("apiKey/APPID=" + textFieldApiKey.getText() + "\n"); //fileWriter.write("%n");
            fileWriter.write("cityIdOrGeoLocation/" + stringCityIdOrGeoLocation + "\n");
            fileWriter.write("cityId/id=" + textFieldCityId.getText() + "\n");
            fileWriter.write("geoLocationLatitude/lat=" + textFieldGeoLocationLatitude.getText() + "\n");
            fileWriter.write("geoLocationLongitude/lon=" + textFieldGeoLocationLongitude.getText() + "\n");
            if(MainController.isInternetConnection()) {
                fileWriter.write("temperatureUnit/" + stringTemperatureUnit + "\n");
            } else {
                fileWriter.write("temperatureUnit/" + stringBeginningSelectedRadioButtonTemperatureUnit + "\n");
            }
            fileWriter.write("checkBoxDescription/" + checkBoxDescription.isSelected() + "\n");
            fileWriter.write("checkBoxHumidity/" + checkBoxHumidity.isSelected() + "\n");
            fileWriter.write("checkBoxTimeOfSunriseAndSunset/" + checkBoxTimeOfSunriseAndSunset.isSelected() + "\n");

            if(checkBoxDescription.isSelected() ) {
                RadioButton selectedRadioButton = (RadioButton) toggleGroupLanguage.getSelectedToggle();
                fileWriter.write("lang/lang=" + selectedRadioButton.getText() + "\n");
            } else {
                fileWriter.write("lang/lang=" + stringBeginningSelectedRadioButtonLanguage + "\n");
            }

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteDecimalPointAtEndOfStringIfExists(TextField textField) {
        String stringTocheck = textField.getText();
        if("".equals(stringTocheck)) {
            return;
        }
        char charToCheck = stringTocheck.charAt(stringTocheck.length() - 1);
        if( (charToCheck == '.' ) || (charToCheck == ',' ) ) {
            String substring = stringTocheck.substring(0, stringTocheck.length() - 1);
            textField.setText(substring);
        }
    }

    private void deleteWhiteSpacesAtBeginningAndEndOf(TextField textField) {
        String stringTocheck = textField.getText();
        if( !("".equals(stringTocheck))) {
            for(int i=0; i < stringTocheck.length(); i++) {
                if(!(Character.isWhitespace(stringTocheck.charAt(i)))) {
                    stringTocheck = stringTocheck.substring(i, stringTocheck.length());
                    textField.setText(stringTocheck);
                    break;
                }
            }
            for(int i=stringTocheck.length()-1; i >= 0; i--) {
                if(!(Character.isWhitespace(stringTocheck.charAt(i)))) {
                    textField.setText(stringTocheck.substring(0, stringTocheck.length() - (stringTocheck.length() - i) + 1));
                    break;
                }
            }
        }
    }

    private void closeWindow() {
        Stage stage = (Stage) vBoxSettings.getScene().getWindow();
        stage.close();
        MainStage.mainController.setVBoxMainNotDisable();
    }

    @FXML
    void buttonWroclawOnAction(ActionEvent event) {
        if(radioButtonCityId.isSelected()) {
            textFieldCityId.setText("3081368"); //   "lon": 17.03, "lat": 51.1
        }
    }
    @FXML
    void buttonGdyniaOnAction(ActionEvent event) {
        if(radioButtonCityId.isSelected()) {
            textFieldCityId.setText("3099424"); // "lon": 18.53, "lat": 54.52
        }
    }
    @FXML
    void buttonWarszawaOnAction(ActionEvent event) {
        if(radioButtonCityId.isSelected()) {
            textFieldCityId.setText("756135"); // "lon": 21.01, "lat": 52.23
        }
    }
}
