package controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import main.MainStage;
import openweathermap.WeatherFormat;

import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.*;
import java.util.HashMap;
import java.util.Map;
import com.mashape.unirest.http.exceptions.UnirestException;
import openweathermap.WeatherFormatDTO;
import openweathermap5day3hourforecast.Lista;
import openweathermap5day3hourforecast.WeatherFormat5Day3Hour;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import openweathermap5day3hourforecast.WeatherFormat5Day3HourDTO;
import others.EnumDayOfWeek;

public class MainController {

    @FXML
    private VBox vBoxMain; // Color RAL 5012
    @FXML
    private ImageView imageViewCurrentWeather;
	@FXML
    private Label labelTemperatureCurrent;
    @FXML
    private Label labelTemperatureCurrentHighest;
    @FXML
    private Label labelTemperatureCurrentLowest;
    @FXML
    private Label labelCity;
    @FXML
    private Label labelDescription;
    @FXML
    private Label labelHumidity;
    @FXML
    private Label labelSunrise;
    @FXML
    private Label labelSunset;
    @FXML
    private Label labelLastWeatherUpdate;
    @FXML
    private HBox hBoxForecast;
    @FXML
    private VBox vBoxEnd;
    @FXML
    private VBox vBoxImageViewCurrentWeather;

    private ObjectMapper mapper;
    private WeatherFormatDTO weatherFormatDTO;
    private WeatherFormat5Day3HourDTO weatherFormat5Day3HourDTO;
    private int temperatureToDisplay = 0;
    final private String DEGREE  = "\u00b0";
    private Image imageToLoad;
    private String GMT;
    private String GMTstring = "GMT+";
    private int GMTvalue;
    private Label labelDay;
    private Label labelForecastHumidity;
    private ImageView imageViewIconCondition;
    private Label labelHighestTemp;
    private Label labelLowestTemp;
    private VBox vBoxForecast;
    private HBox tempHBox;
    private Line line;
    private ContextMenu generalContextMenu;
    private Menu opacityMenu;
    private CheckMenuItem[] opacityCheckMenuItem;
    private Map<String,String> mapForFileSettings = new HashMap<String,String>();
    private Map<String,String> mapForFileData = new HashMap<String,String>();
    private Map<String,String> mapForFilePosition = new HashMap<String,String>();
    private String stringCountry;
    private int[][] tabTemperaturePrepared;
    private String[] tabForecastDays = new String[5];
    private String temperatureUnit;
    private CheckMenuItem checkMenuItemChildBig;
    private CheckMenuItem checkMenuItemChildSmall;
    private static boolean isInternetConnection = false;
    private double positionX = 0;
    private double positionY = 0;
    private long unixTimeFromJson;
    private int latencyUpdateDays;
    private int latencyUpdateTimeHours;
    private int latencyUpdateTimeMinutes;
//    private boolean isCityGeoLocationTheSame;

    private final String PATH_FILE_SETTINGS = "./src/main/resources/txt/widgetSettings.txt";
    private final String PATH_FILE_DATA = "./src/main/resources/txt/widgetData.txt";
    private final String PATH_FILE_POSITION = "./src/main/resources/txt/widgetPosition.txt";
    private final String PATH_FOLDER_SAVEDIMAGES = "./src/main/resources/savedImages";

    @FXML
    public void initialize() {
        System.out.println("Start");
//  TODO for first usage write that there is no API and key id setted
// TODO write info if weather was not updated for only 5 days cast (ex. do not recognize some field)

//        File myFile = new File(PATH_FILE_POSITION);
//        if(!myFile.exists()) { // file "widgetPosition.txt" is created after
//            labelTemperatureCurrent.setText("No API key");
//        }

//        emptyOutFolder(PATH_FOLDER_SAVEDIMAGES);

        File myFile = new File(PATH_FILE_SETTINGS);
        if(!myFile.exists()) {
            try {
                FileWriter fileWriter = new FileWriter(PATH_FILE_SETTINGS);
                fileWriter.write("apiKey/APPID=\n"); //fileWriter.write("%n");
                fileWriter.write("cityIdOrGeoLocation/id=\n");
                fileWriter.write("cityId/id=\n");
                fileWriter.write("geoLocationLatitude/lat=\n");
                fileWriter.write("geoLocationLongitude/lon=\n");
                fileWriter.write("temperatureUnit/units=metric\n");
                fileWriter.write("checkBoxDescription/true\n");
                fileWriter.write("checkBoxHumidity/true\n");
                fileWriter.write("checkBoxTimeOfSunriseAndSunset/true\n");
                fileWriter.write("lang/lang=en\n");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        copyDataFromFileTo(mapForFileSettings, PATH_FILE_SETTINGS);
        createGeneralContextMenu();
        fillHBoxEnd();

        copyDataFromFileTo(mapForFilePosition, PATH_FILE_POSITION);
        if(mapForFilePosition.get("widthPrimaryStage") != null) {
            if(Double.parseDouble(mapForFilePosition.get("widthPrimaryStage")) == MainStage.getWidthSmallPrimaryStage()) {
                checkMenuItemChildBig.setSelected(false);
                checkMenuItemChildSmall.setSelected(true);
            }
        }
        if(mapForFilePosition.get("opacityPrimaryStage") != null) {
            String opacityPrimaryStage = mapForFilePosition.get("opacityPrimaryStage");
            double vDouble = Double.parseDouble(opacityPrimaryStage);
            int vInt = (int) (vDouble * 100);
            String stringToCompare = "" + vInt + "%";
            for (int i=0; i < opacityCheckMenuItem.length; i++){
                if(opacityCheckMenuItem[i].getText().equals(stringToCompare) ) {
                    opacityCheckMenuItem[i].setSelected(true);
                    opacityCheckMenuItem[opacityCheckMenuItem.length - 1].setSelected(false);
                    break;
                }
            }
        }

//        setWeatherValuesFromFile();

        //------------------------------------------------------------------------------
        // update values from .json files
//        readValuesFromWeatherFormatDTO();
//        currentWeather();
//        readValuesFromWeatherFormat5Day3HourDTO();
//        forecastForOtherDays();

//        isInternetConnection = true;
        //------------------------------------------------------------------------------



        tempThread.start();
    }

    private void emptyOutFolder(String pathFolderToEmptyOut) {
        Platform.runLater(new Runnable() {
            @Override
            public synchronized void run() {
                File directory = new File(String.valueOf(pathFolderToEmptyOut));
                if (directory.exists()) {
                    deleteFolder(directory);
                }
                directory.mkdir();
            }
        });
    }

    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) {
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
//        System.out.println("(#MainController) deleteFolder(File folder)");
    }
//
    private void setWeatherValuesFromFile() {
        File myFile;
        myFile = new File(PATH_FILE_DATA);
        if(myFile.exists()) {
            copyDataFromFileTo(mapForFileData, PATH_FILE_DATA);
            labelTemperatureCurrent.setText(mapForFileData.get("temperatureCurrent"));
            labelTemperatureCurrentHighest.setText(mapForFileData.get("temperatureCurrentHighest") + "  ");
            labelTemperatureCurrentLowest.setText(mapForFileData.get("temperatureCurrentLowest"));
            labelCity.setText(mapForFileData.get("city"));
            labelDescription.setText(mapForFileData.get("description"));

            labelHumidity.setText(mapForFileData.get("humidity"));
            labelSunrise.setText(mapForFileData.get("sunrise") + "   ");
            labelSunset.setText(mapForFileData.get("sunset"));
            for(int i=1; i < 5; i++) {
                createControlsForForecast();
                labelDay.setText(mapForFileData.get("day" + i));

                labelHighestTemp.setText(mapForFileData.get("highestTemp" + i) + DEGREE + "  ");
                labelLowestTemp.setText(mapForFileData.get("lowestTemp" + i) + DEGREE);
            }
        }
    }

    private Thread tempThread = new Thread(){
        public void run(){
//            System.out.println("(#MainController) start tempThread");
//            readValuesFromWeatherFormatDTO();
//            currentWeather();
//            readValuesFromWeatherFormat5Day3HourDTO();
//            forecastForOtherDays();

//            runSystemCommand("ping stackoverflow.com");

//            System.out.println("(#MainController) start start tempThread");
//            System.out.println("(#MainController) forecastForOtherDays() ZMIANA isInternetConnection NA TRUE");
//            isInternetConnection = true;
//            int delayValue = 1;
//            System.out.println("(#MainController) start delay " + delayValue);

//            System.out.println("(#MainController) end delay " + delayValue);
//            isInternetConnection = true;
            for(;;){
                try {
//                    runSystemCommand("ping stackoverflow.com");
                    updateValues();

                    Thread.sleep(45_000); // 10 min -> 600_000
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException \n");
                    e.printStackTrace();
                }
            } // koniec for
        }
    };

    public void updateValues() {
//        runSystemCommand("ping stackoverflow.com");
        if (isInternetConnection()) {
            emptyOutFolder(PATH_FOLDER_SAVEDIMAGES);
        }
        if(isApiKeyAndCityCorrectlySetted()) {
            currentWeather();
            updateWeatherFormat5Day3HourDTO();
            forecastForOtherDays();
            if(!isCityGeoLocationTheSame()) {
                System.out.println("(#MainController) updateValues(), isCityGeoLocationTheSame() = false, NO");
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        labelTemperatureCurrent.setWrapText(true);
                        labelTemperatureCurrent.setMaxWidth(300);
                        labelTemperatureCurrent.setText("No internet connection"); // Exception in thread "Thread-3" java.lang.IllegalStateException: Not on FX application thread; currentThread = Thread-3
                    }
                });
            } else {
                System.out.println("(#MainController) updateValues(), isCityGeoLocationTheSame() = true");
            }
//            saveWeatherValuesToFile(PATH_FILE_DATA);
        }
    }

//    private void saveWeatherValuesToFile(String path_file_data) {
//        Platform.runLater(new Runnable() {
//            @Override
//            public synchronized void run() {
//                File myFile = new File(path_file_data);
//                myFile.delete();
//                if(!myFile.exists()) {
//                    try {
//                        FileWriter fileWriter = new FileWriter(path_file_data);
//                        fileWriter.write("GMT/" + GMT + "\n");
//                        fileWriter.write("temperatureCurrent/" + temperatureToDisplay + DEGREE + temperatureUnit + "\n");
//                        fileWriter.write("temperatureCurrentHighest/" + labelTemperatureCurrentHighest.getText() + "\n");
//                        fileWriter.write("temperatureCurrentLowest/" + labelTemperatureCurrentLowest.getText() + "\n");
//                        fileWriter.write("city/" + labelCity.getText() + "\n");
//                        fileWriter.write("description/" + labelDescription.getText() +  "\n");
//                        fileWriter.write("humidity/" + labelHumidity.getText() + "\n");
//                        fileWriter.write("sunrise/" + labelSunrise.getText() + "\n");
//                        fileWriter.write("sunset/" + labelSunset.getText() + "\n");
//
//                        for(int i=1; i<5; i++) {
//                            fileWriter.write("day" + i + "/" + tabForecastDays[i] + "\n");
//                            fileWriter.write("highestTemp" + i + "/" + tabTemperaturePrepared[i][0] + "\n");
//                            fileWriter.write("lowestTemp" + i +  "/" + tabTemperaturePrepared[i][1] + "\n");
//                        }
////                        fileWriter.write("height/" + MainStage.getPrimaryStage().getHeight() + "\n");
////                        fileWriter.write("width/" + MainStage.getPrimaryStage().getWidth() + "\n");
//                        fileWriter.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//
//    }

    private boolean isApiKeyAndCityCorrectlySetted() {
        updateWeatherFormatDTO();
        int cod = weatherFormatDTO.getCod();
        String messageFromApi = weatherFormatDTO.getMessage();
//        cod=400, message='Nothing to geocode'
//        cod=401, message='Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.'
//        cod=404, message='city not found'
//        cod=429, message='Too many requests'
//        cod=500, message='Internal server error'
        if((cod >= 400) && (cod <= 500) ){
            resetWeatherValues(cod,messageFromApi);
            return false;
        }
        return true;
    }

    private synchronized void resetWeatherValues(int cod, String info) {
        labelTemperatureCurrentHighest.setText("");
        labelTemperatureCurrentLowest.setText("");
        labelCity.setText("");
        labelDescription.setText("");
        labelHumidity.setText("");
        labelSunrise.setText("");
        labelSunset.setText("");
        labelLastWeatherUpdate.setText("");
        imageViewCurrentWeather.setImage(null);
        vBoxImageViewCurrentWeather.setPrefSize(0,0);
        // 2
        hBoxForecast.getChildren().clear(); // 2
        if( (cod >= 400) && (cod <= 500) ) {
            labelTemperatureCurrent.setText("Error " + cod);
            setErrorInfo(info);
        } else {
            System.out.println("\t(#MainController) resetWeatherValues cod = " + 0);
            labelTemperatureCurrent.setText("");
        }
        System.out.println("\t(#MainController) resetWeatherValues end function, cod = " + cod);
    }

    private void setErrorInfo(String s) {
        Label labelMessage = new Label(s);
        labelMessage.setTextFill(Color.WHITE);
        labelMessage.setWrapText(true);
        labelMessage.setMaxWidth(300);
        labelMessage.setTextFill(Color.YELLOW);
        hBoxForecast.getChildren().addAll(labelMessage);
    }

    private void copyDataFromFileTo(Map map, String pathFile) {
        try {
            FileReader myFileReader = new FileReader(pathFile);
            BufferedReader myBufferedReader = new BufferedReader(myFileReader);
            String textFromFile = "";

            while((textFromFile = myBufferedReader.readLine()) != null) {
                String[] stringTab = textFromFile.split("/");
                map.put(stringTab[0], stringTab[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void currentWeather() {
//        System.out.println("(#MainController) start currentWeather()");
        setGMT();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                resetWeatherValues(0, "");
                int latencyDays = checkWeatherUpdateLatency();
                System.out.println("(#MainController) latencyDays = " + latencyDays);
                String imageURL;

                if(isCityGeoLocationTheSame()) {
                    if (latencyDays == 0) {
                        copyDataFromFileTo(mapForFileSettings, PATH_FILE_SETTINGS);
                        temperatureToDisplay = (int) weatherFormatDTO.getMain().getTemp();
                        String stringToCompare = mapForFileSettings.get("temperatureUnit").substring(6);
                        temperatureUnit = "";
//                        if (isInternetConnection) {
                            if ("metric".equals(stringToCompare)) {
                                temperatureUnit = "C";
                            } else if ("standard".equals(stringToCompare) || "".equals(stringToCompare)) {
                                temperatureUnit = "K";
                            } else if ("imperial".equals(stringToCompare)) {
                                temperatureUnit = "F";
                            }
//                        } else {
//                            temperatureUnit = "C";
//                        }
                        if (latencyDays == 0 && latencyUpdateTimeHours < 2) {
                            labelTemperatureCurrent.setMaxWidth(Region.USE_COMPUTED_SIZE);
                            labelTemperatureCurrent.setText("" +
                                    temperatureToDisplay +
                                    DEGREE + temperatureUnit);
                        }


                        if (isInternetConnection()) {
//                            emptyOutFolder(PATH_FOLDER_SAVEDIMAGES); //
                            imageURL = "http://openweathermap.org/img/wn/" + weatherFormatDTO.getWeather()[0].getIcon() + "@2x.png";
                            System.out.println("\t\t(#MainController) image to save 0.png ");
                            saveImageToDirectory(imageURL, PATH_FOLDER_SAVEDIMAGES + "/" + 0 + ".png");

//                            imageURL = "http://openweathermap.org/img/wn/" + iconForecast + "@2x.png";
//                            saveImageToDirectory(imageURL, PATH_FOLDER_SAVEDIMAGES + "/" + dayCount + ".png");
//                            System.out.println("\t\t(#MainController) image setted with internet: " + imageURL);
                        } else {
                            if (latencyUpdateTimeHours < 5) {
                                imageURL = "/savedImages/0.png";
                                System.out.println("\t\t(#MainController) image setted without internet: " + imageURL);
                            } else {
                                imageURL = null;
                            }
//                    imageURL = getClass().getResource("./src/main/resources/savedImages/0.png");
                        }
                        System.out.println("(#MainController) isInternetConnection = " + isInternetConnection + "");
//                    System.out.println("(#MainController) currentWeather()       imageURL = " + imageURL + "");
//                    imageToLoad = new Image(imageURL);

                        vBoxImageViewCurrentWeather.setPrefSize(100, 100);
                        if (imageURL != null) {
                            imageViewCurrentWeather.setImage(new Image(imageURL));
                        }
//                labelTemperatureCurrentHighest.setText("highest ");
//                labelTemperatureCurrentLowest.setText("lowest");
//                    String coma = ",";
//                    if(weatherFormatDTO.getSys().getCountry() == null) {
//                        weatherFormatDTO.getSys().setCountry("");
//                        coma = "";
//                    }
//                    if("".equals(weatherFormatDTO.getName())) {
//                        labelCity.setText("Coord: " + weatherFormatDTO.getCoord().getLat() + ";" + weatherFormatDTO.getCoord().getLon()); //  the line of latitude is always given first followed by the line of longitude
//                    } else {
//                        stringCountry = weatherFormatDTO.getSys().getCountry();
//                        labelCity.setText(weatherFormatDTO.getName() + coma + stringCountry);
//                    }

                        labelDescription.setText(weatherFormatDTO.getWeather()[0].getDescription());
                        labelHumidity.setText("" + (int) weatherFormatDTO.getMain().getHumidity() + "%");

                        long unixTimeFromJson = weatherFormatDTO.getSys().getSunrise();
                        String hourUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getHour();
                        String minuteUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getMinute();
                        if (minuteUnix.length() == 1) {
                            minuteUnix = "0" + minuteUnix;
                        }
                        labelSunrise.setText("sunrise " + hourUnix + ":" + minuteUnix + "   ");

                        unixTimeFromJson = weatherFormatDTO.getSys().getSunset();
                        hourUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getHour();
                        minuteUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getMinute();
                        if (minuteUnix.length() == 1) {
                            minuteUnix = "0" + minuteUnix;
                        }
                        labelSunset.setText("sunset " + hourUnix + ":" + minuteUnix);

                    } else {
                        imageURL = "/savedImages/" + latencyDays + ".png";
                        imageViewCurrentWeather.setImage(new Image(imageURL));
//                    System.out.println("(#MainController) image setted without internet: " + imageURL);
                    }
                } else {
                    resetWeatherValues(0,"");
                    labelTemperatureCurrent.setText("");
                    System.out.println("\n\nisCityGeoLocationTheSame() = false\n\tlabelTemperatureCurrent.getText() = " + labelTemperatureCurrent.getText() + "\n");
                    String cityIdOrGeoLocation = mapForFileSettings.get("cityIdOrGeoLocation");
//                    labelCity.setText();
                }

                String coma = ",";
                if(weatherFormatDTO.getSys().getCountry() == null) {
                    weatherFormatDTO.getSys().setCountry("");
                    coma = "";
                }
                if(isCityGeoLocationTheSame()) {
                    if("".equals(weatherFormatDTO.getName())) {
                        labelCity.setText("Coord: " + weatherFormatDTO.getCoord().getLat() + ";" + weatherFormatDTO.getCoord().getLon()); //  the line of latitude is always given first followed by the line of longitude
                    } else {
                        stringCountry = weatherFormatDTO.getSys().getCountry();
                        labelCity.setText(weatherFormatDTO.getName() + coma + stringCountry);
                    }
                }

                if("true".equals(mapForFileSettings.get("checkBoxDescription")) ) {
                    labelDescription.setVisible(true);
                } else { //if("false".equals(mapForFileSettings.get("checkBoxDescription")) )
                    labelDescription.setVisible(false);
                }

                if("true".equals(mapForFileSettings.get("checkBoxHumidity")) ) {
                    labelHumidity.setVisible(true);
                } else {
                    labelHumidity.setVisible(false);
                }

                if("true".equals(mapForFileSettings.get("checkBoxTimeOfSunriseAndSunset")) ) {
                    labelSunrise.setVisible(true);
                    labelSunset.setVisible(true);
                } else {
                    labelSunrise.setVisible(false);
                    labelSunset.setVisible(false);
                }
            }
        });
//        System.out.println("(#MainController) end currentWeather()");
    }

    private boolean isCityGeoLocationTheSame() {
//        isCityGeoLocationTheSame = false;
        copyDataFromFileTo(mapForFileSettings, PATH_FILE_SETTINGS);
        String stringCityGeo = mapForFileSettings.get("cityIdOrGeoLocation");
        char tempchar = stringCityGeo.charAt(0);
        if('i' == tempchar ) {
//                    System.out.println("\t\t(#MainController) stringCityGeo.substring(3, stringCityGeo.length() - 1) = " + stringCityGeo.substring(3, stringCityGeo.length() - 1));
            if(stringCityGeo.substring(3, stringCityGeo.length()).equals("" + weatherFormatDTO.getId())) {
                System.out.println("\t(#MainController) id isCityGeoLocationTheSame() = " + true);
                return true;
            }
        } else if('l' == tempchar) {
            String[] str = stringCityGeo.split("&lon="); // lat=
            double parseDoubleLat = Double.parseDouble(str[0].substring(4, str[0].length()));
            double parseDoubleLon = Double.parseDouble(str[1]);
            System.out.println("(#MainController) \tparseDoubleLat = " + parseDoubleLat + "\tparseDoubleLon = " + parseDoubleLon);
            if(parseDoubleLat == weatherFormatDTO.getCoord().getLat() && parseDoubleLon == weatherFormatDTO.getCoord().getLon()) {
                System.out.println("\t(#MainController) lat lon isCityGeoLocationTheSame() = " + true );
                return true;
            }
        }
        System.out.println("(#MainController) isCityGeoLocationTheSame() = " + false);
        return false;
    }

    private int checkWeatherUpdateLatency() {
        String hourUnix;
        String minuteUnix;
        unixTimeFromJson = weatherFormatDTO.getDt();
        String dayOfWeekUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getDayOfWeek();
        String yearUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getYear();
        String monthUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getMonth();
        String dayOfMonth = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getDayOfMonth();
        hourUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getHour();
        minuteUnix = "" + Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getMinute();

        if(minuteUnix.length() == 1) {
            minuteUnix = "0" + minuteUnix;
        }

//                labelLastWeatherUpdate.setText("last update " + dayOfWeekUnix.substring(0,3) + ", "
//                        + yearUnix + "/"  + monthUnix.substring(0,3) + "/" + dayOfMonth + ", "
//                        + hourUnix + ":" + minuteUnix);
        System.out.println("(#MainController) last update " + dayOfWeekUnix.substring(0,3) + ", "
                        + yearUnix + "/"  + monthUnix.substring(0,3) + "/" + dayOfMonth + ", "
                        + hourUnix + ":" + minuteUnix);
        //------------------------------------


//                long timeMillis = Calendar.getInstance().getTimeInMillis()/1000;
//                int hour1 = Instant.ofEpochSecond(timeMillis).atZone(ZoneId.of(GMTstring + GMTvalue)).getHour();
//                int minute1 = Instant.ofEpochSecond(timeMillis).atZone(ZoneId.of(GMTstring + GMTvalue)).getMinute();
//                System.out.println("\n\ntimeMillis = " + hour1 + ":" + minute1);
//
//                int lengthOfYear = LocalDate.now().lengthOfYear();
//                Era era = LocalDate.now().getEra();
//                int dayOfYear = LocalDateTime.now().getDayOfYear();
//                int hour2 = LocalDateTime.now().getHour();
//                int minute2 = LocalDateTime.now().getMinute();
//                System.out.println(era + ", " + lengthOfYear + " - " +  dayOfYear + ", " + hour2 + ":" + minute2);
//                LocalDateTime.now();
//                System.out.println("(#MainController) LocalDateTime.now(); = " + LocalDateTime.now());
//                LocalDate.now();
//                System.out.println("(#MainController) LocalDate.now() = " + LocalDate.now());
//                LocalTime.now();
//                System.out.println("(#MainController) LocalTime.now() = " + LocalTime.now());
//

        //----------------------------------------
        LocalDateTime currentLocalDateTime = LocalDateTime.now(ZoneId.of(GMT));
        int currentDayOfYear = currentLocalDateTime.getDayOfYear();
//                System.out.println("#(MainController) currentDayOfYear  = " + currentDayOfYear );
        int currentYear = currentLocalDateTime.getYear();
        int currentHour = currentLocalDateTime.getHour();
        int currentMinute = currentLocalDateTime.getMinute();

//                int valueDayOfWeek = LocalDate.now().getDayOfWeek().getValue();             //
//                System.out.println("VALUE valueDayOfWeek = " + valueDayOfWeek);
//                EnumDayOfWeek en = EnumDayOfWeek.MON; //en.getValue(valueDayOfWeek);
//                System.out.println("ENUM " + en);
//                EnumDayOfWeek weekDay = EnumDayOfWeek.MON.getValue(valueDayOfWeek);         //
//                System.out.println("ENUM ofWeek " + weekDay);

//                String dayOfWeek = "" + LocalDate.now().getDayOfWeek();
//                dayOfWeek = dayOfWeek.substring(0,3);
//                System.out.println("CURRENT dayOfWeek = " + dayOfWeek);

        int lastUpdateDayOfYear = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getDayOfYear();
//                System.out.println("#(MainController) lastUpdateDayOfYear = " + lastUpdateDayOfYear );
        int yearFromWeahterFileJson = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getYear();
//                System.out.println("(#MainController) yearFromWeahterFileJson = " + yearFromWeahterFileJson);
//                int lengthOfYearFromWeahterFileJson = LocalDate.now().withDayOfYear(2019).lengthOfYear();
        LocalDate temporaryLocalDate = LocalDate.of(yearFromWeahterFileJson, 1, 1);
//                System.out.println("(#MainController) temporaryLocalDate = " + temporaryLocalDate);
//                int lengthOfYear1 = temporaryLocalDate.lengthOfYear();
        int lengthOfYearFromWeahterFileJson = temporaryLocalDate.lengthOfYear();
        ;
//                System.out.println("(#MainController) lengthOfYearFromWeahterFileJson = " + lengthOfYearFromWeahterFileJson);
        int lastUpdateTimeHour = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getHour();
        int lastUpdateTimeMinute = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getMinute();

        latencyUpdateDays = 0;
        if((currentYear == yearFromWeahterFileJson)) { // (currentDayOfYear >= lastUpdateDayOfYear) &&
            latencyUpdateDays = currentDayOfYear - lastUpdateDayOfYear;
        } else if(currentYear == (yearFromWeahterFileJson + 1)) {
            latencyUpdateDays = (currentDayOfYear + lengthOfYearFromWeahterFileJson) - lastUpdateDayOfYear;
        }
//                else {
//                    latencyUpdateDays = 999;
//                }
        latencyUpdateTimeHours = currentHour - lastUpdateTimeHour;
        latencyUpdateTimeMinutes = currentMinute - lastUpdateTimeMinute;

                System.out.println("\t\n(#MainController) latencyUpdateDays = " + latencyUpdateDays
                        + " | latencyUpdateTimeHours = " + latencyUpdateTimeHours
                        + " | latencyUpdateTimeMinutes = " + latencyUpdateTimeMinutes );

//                labelLastWeatherUpdate.setTextFill(Color.WHITE);
        if((latencyUpdateDays == 0) && (latencyUpdateTimeHours == 0) && latencyUpdateTimeMinutes <= 29 ) {
            labelLastWeatherUpdate.setText("");
        } else {
            if(latencyUpdateDays > 0 ) {
                String stringDay = "day";
                if(latencyUpdateTimeHours > 1) {
                    stringDay = stringDay + "s";
                }
                labelLastWeatherUpdate.setText("last update: " + latencyUpdateDays + " " + stringDay);
            } else if(latencyUpdateTimeHours > 0) {
                String stringHour = "hour";
                if(latencyUpdateTimeHours == 1 && latencyUpdateTimeMinutes < 0) {
                    labelLastWeatherUpdate.setText("");
                } else if(latencyUpdateTimeHours > 1) {
                    stringHour = stringHour + "s";
                    labelLastWeatherUpdate.setText("last update: " + latencyUpdateTimeHours + " " + stringHour);
                }
            } else if(latencyUpdateTimeMinutes > 29) {
                labelLastWeatherUpdate.setText("last update: " + latencyUpdateTimeMinutes + " minutes");
            }
//                    labelLastWeatherUpdate.setTextFill(Color.RED);
        }
        return latencyUpdateDays;
    }

    public void forecastForOtherDays() {
//        System.out.println("(#MainController) start forecastForOtherDays()");
//        updateWeatherFormat5Day3HourDTO();
        setGMT();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int dayCounter = 0;
                int counterZero = 0;
                Lista tempLista;
                String stringDateTimeToPut;
                LocalDateTime parse;
                int temperatureFromList;
                int[][] tabWithAllForecastTemperature = new int[weatherFormat5Day3HourDTO.getList().length][2];

                for(int i=0; i < weatherFormat5Day3HourDTO.getList().length; i++ ) {
                    tempLista = weatherFormat5Day3HourDTO.getList()[i];
                    stringDateTimeToPut = tempLista.getDt_txt();
                    stringDateTimeToPut = stringDateTimeToPut.replaceAll(" ","T");
                    parse = LocalDateTime.parse(stringDateTimeToPut);

                    if(parse.getHour() == 6 && i > 0) {
                        dayCounter++;
                    }
                    if(parse.getHour() == 0) {
                        counterZero++;
                    }
                    tabWithAllForecastTemperature[i][0] = dayCounter;
                    temperatureFromList = (int) tempLista.getMain().getTemp();
                    tabWithAllForecastTemperature[i][1] = temperatureFromList;
                }

                tabTemperaturePrepared = new int[6][2];
                if(counterZero == 8) {
                    tabTemperaturePrepared = new int[5][2];
                }

                // get lowest and bigest temperature value
                int counter = 0;
                int temperatureMAX = -999;
                int temperatureMIN = 999;

                for(int k=0; k < tabWithAllForecastTemperature.length; k++) {
                    while(tabWithAllForecastTemperature[k][0] == counter) {
                        if(tabWithAllForecastTemperature[k][1] > temperatureMAX) {
                            temperatureMAX = tabWithAllForecastTemperature[k][1];
                        }
                        if(tabWithAllForecastTemperature[k][1] < temperatureMIN) {
                            temperatureMIN = tabWithAllForecastTemperature[k][1];
                        }
                        k++;
                        if( k >= tabWithAllForecastTemperature.length) {
                            break;
                        }
                    }
                    k--;
                    tabTemperaturePrepared[counter][0] = temperatureMAX;
                    tabTemperaturePrepared[counter][1] = temperatureMIN;
                    counter++;
                    temperatureMAX = -999;
                    temperatureMIN = 999;
                }

                for(int i=0 ; i < tabTemperaturePrepared.length; i++) {
//                    System.out.println("(#MainController) tabTemperaturePrepared[" + i + "][0] = " + tabTemperaturePrepared[i][0] + "");
//                    System.out.println("(#MainController) tabTemperaturePrepared[" + i + "][1] = " + tabTemperaturePrepared[i][1] + "");
                }

                // ----------
                System.out.println("City = " + weatherFormat5Day3HourDTO.getCity().getCountry());
                Lista[] listaForecast = weatherFormat5Day3HourDTO.getList();
                hBoxForecast.getChildren().clear();

                String dt_txt = "";
                int dayCount = 1;

                int latencyDays = checkWeatherUpdateLatency();
                System.out.println("(#MainController) latencyDays = " + latencyDays + "");
                if(isInternetConnection()) { // if(latencyDays == 0 ) {
                    if(tabTemperaturePrepared[0][0] > temperatureToDisplay) {
                        labelTemperatureCurrentHighest.setText("" + tabTemperaturePrepared[0][0] + DEGREE);
                    } else {
                        labelTemperatureCurrentHighest.setText("" + temperatureToDisplay + DEGREE);
                    }
                    labelTemperatureCurrentLowest.setText("  " + tabTemperaturePrepared[0][1] + DEGREE);
                } else {
                    if(isCityGeoLocationTheSame()) {
                        labelTemperatureCurrentHighest.setText("" + tabTemperaturePrepared[latencyDays][0] + DEGREE);
                        labelTemperatureCurrentLowest.setText("  " + tabTemperaturePrepared[latencyDays][1] + DEGREE);
                    }
                }


                String iconForecast = "";
//                hBoxForecast.getChildren().clear();
//                int valueDayOfWeek = LocalDate.now().getDayOfWeek().getValue();
                //
                DayOfWeek dayOfWeekTEST = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getDayOfWeek();
                System.out.println("(#MainController) dayOfWeekTEST = " + dayOfWeekTEST);
//                int valueDayOfWeek = Instant.ofEpochSecond(unixTimeFromJson).atZone(ZoneId.of(GMT)).getDayOfWeek().getValue();
                int valueDayOfWeek = LocalDate.now(ZoneId.of(GMT)).getDayOfWeek().getValue();

                System.out.println("(#MainController) valueDayOfWeek = " + valueDayOfWeek);
                //

                int numberHowManyDaysDisplay = 5;
                numberHowManyDaysDisplay = numberHowManyDaysDisplay - latencyDays;
                for(int i=1 ; i < listaForecast.length; i++ ) {
                    dt_txt = listaForecast[i].getDt_txt();
                    if(dt_txt.charAt(11) == '1' && dt_txt.charAt(12) == '2' ) {
                        iconForecast = weatherFormat5Day3HourDTO.getList()[i].getWeather()[0].getIcon();
                        createControlsForForecast();

                        valueDayOfWeek = valueDayOfWeek + 1;
                        if(valueDayOfWeek > 7) {
                            valueDayOfWeek = 1;
                        }
                        EnumDayOfWeek weekDay = EnumDayOfWeek.MON.getValue(valueDayOfWeek);

                        labelDay.setText("" + weekDay);
//                        tabForecastDays[dayCount] = labelDay.getText();
//                        latencyDays = checkWeatherUpdateLatency();
//                        tabForecastDays[dayCount] = labelDay.getText();
//  option                      labelForecastHumidity.setText("" + (int) weatherFormat5Day3HourDTO.getList()[i].getMain().getHumidity() + "%");
//                        latencyDays = checkWeatherUpdateLatency();
                        System.out.println("(#MainController) latencyDays = " + latencyDays + "");
                        if(numberHowManyDaysDisplay > dayCount) { // if(latencyDays >= dayCount) { // previous <=

                            String imageURL;
                            int v2;
                            imageViewIconCondition.setFitHeight(55);
                            imageViewIconCondition.setFitWidth(55);
                            if(isInternetConnection()) {
                                imageURL = "http://openweathermap.org/img/wn/" + iconForecast + "@2x.png";
                                saveImageToDirectory(imageURL, PATH_FOLDER_SAVEDIMAGES + "/" + dayCount + ".png");
                                labelHighestTemp.setText("" + tabTemperaturePrepared[dayCount][0] + DEGREE + "  "); // TODO save highest value from current day and display it
                                labelLowestTemp.setText("" + tabTemperaturePrepared[dayCount][1] + DEGREE); // TODO save lowest value from current day and display it
                            } else {
                                v2 = latencyDays + dayCount;
                                imageURL = "/savedImages/" + v2 + ".png";//dayCount "./src/main/resources/savedImages/0.png"
                                labelHighestTemp.setText("" + tabTemperaturePrepared[v2][0] + DEGREE + "  ");
                                labelLowestTemp.setText("" + tabTemperaturePrepared[v2][1] + DEGREE);
                                if(dayCount == 5) {
                                    isInternetConnection = true;
                                }
                            }
                            System.out.println("(#MainController) forecastForOtherDays() imageURL = " + imageURL + "");
                            imageViewIconCondition.setImage(new Image(imageURL));

//                        try(InputStream in = new URL("http://example.com/image.jpg").openStream()){
//                            Files.copy(in, Paths.get("C:/File/To/Save/To/image.jpg"));
//                        }

//                            imageViewIconCondition.setFitHeight(55);
//                            imageViewIconCondition.setFitWidth(55);
//                            labelHighestTemp.setText("" + tabTemperaturePrepared[v2][0] + DEGREE + "  ");
//                            labelLowestTemp.setText("" + tabTemperaturePrepared[v2][1] + DEGREE);

                        }


                        dayCount++;
                        if(dayCount == 5) { // if(dayCount == 5) {
                            System.out.println();
                           break;
                        }
                        hBoxForecast.getChildren().add(line);
                   }
                } // koniec for
            }
        });
//        System.out.println("(#MainController) end forecastForOtherDays()");
    }

    private void saveImageToDirectory(String imageURL, String imagePath) {
        try(InputStream in = new URL(imageURL).openStream()){
            try {
                Files.copy(in, Paths.get(imagePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setGMT() {
        GMTvalue = weatherFormatDTO.getTimezone() / 3600;
        GMTstring = "GMT+";
        if(GMTvalue < 0) {
            GMTstring = "GMT";
        }
        GMT = GMTstring + GMTvalue;
    }

    private void createControlsForForecast() {
        labelDay = new Label("day");
        Font fontLabelDay = new Font(12);
        labelDay.setFont(fontLabelDay);
        labelDay.setTextFill(Color.WHITE);
//        labelForecastHumidity = new Label("humidity");
//        labelForecastHumidity.setTextFill(Color.WHITE);

        imageViewIconCondition = new ImageView();

        labelHighestTemp = new Label(""); // high
        labelHighestTemp.setFont(new Font(12));
        labelHighestTemp.setTextFill(Color.WHITE);
        labelLowestTemp = new Label(""); // low
        labelLowestTemp.setFont(new Font(12));
        labelLowestTemp.setTextFill(Color.BLACK);
        tempHBox = new HBox();
        tempHBox.setAlignment(Pos.CENTER);
        tempHBox.getChildren().addAll(labelHighestTemp, labelLowestTemp);
        line = new Line(0, 0,   0,   95);
        line.setStroke(Color.SILVER);

        vBoxForecast = new VBox();
        vBoxForecast.setPrefWidth(73);
        vBoxForecast.setAlignment(Pos.TOP_CENTER);
        vBoxForecast.setPadding(new Insets(5,0,5,0));
        if(isCityGeoLocationTheSame()) {
            vBoxForecast.getChildren().addAll(labelDay, imageViewIconCondition, tempHBox); // labelForecastHumidity,
        }
        hBoxForecast.getChildren().add(vBoxForecast);
    }

    private void updateWeatherFormatDTO() {
        try {
            if(isInternetConnection) {
                weatherFormatDTO = new WeatherFormat().getAll();
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        readValuesFromWeatherFormatDTO();
    }

    private void readValuesFromWeatherFormatDTO() {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            weatherFormatDTO = mapper.readValue(new File("data/weather.json"), WeatherFormatDTO.class);
        } catch (FileNotFoundException e) {
            System.out.println("(#MainController) readValuesFromWeatherFormatDTO() FileNotFoundException.");
            labelTemperatureCurrent.setText("no internet");
            e.printStackTrace();
        } catch (com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException e) {
            System.out.println("(#MainController) updateWeatherFormatDTO() UnrecognizedPropertyException.");
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    labelTemperatureCurrent.setText("Error ");
                    setErrorInfo("UnrecognizedPropertyException Current");
                }
            });
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("(#MainController) readValuesFromWeatherFormatDTO() Brak połączenia z internetem.");
            e.printStackTrace();
        }
    }

    private void updateWeatherFormat5Day3HourDTO() {
        try {
            if(isInternetConnection) {
                weatherFormat5Day3HourDTO = new WeatherFormat5Day3Hour().getAll();
            }
        } catch (UnirestException e) {
            System.out.println("(#MainController) updateWeatherFormat5Day3HourDTO() Brak połączenia z internetem.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        readValuesFromWeatherFormat5Day3HourDTO();
    }

    private void readValuesFromWeatherFormat5Day3HourDTO() {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            weatherFormat5Day3HourDTO = mapper.readValue(new File("data/weather5Day3Hour.json"),WeatherFormat5Day3HourDTO.class);
        } catch (FileNotFoundException e) {
            System.out.println("(#MainController) readValuesFromWeatherFormat5Day3HourDTO() FileNotFoundException.");
            e.printStackTrace();
        } catch (com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException e) {
            System.out.println("(#MainController) updateWeatherFormat5Day3HourDTO() UnrecognizedPropertyException.");
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    setErrorInfo("UnrecognizedPropertyException 5Day");
                }
            });
            e.printStackTrace();
        }  catch (IOException e) {
            System.out.println("(#MainController) readValuesFromWeatherFormat5Day3HourDTO() Brak połączenia z internetem.");
            e.printStackTrace();
        }
    }

    private void fillHBoxEnd() {
        Hyperlink hyperlink = new Hyperlink("https://openweathermap.org");
        hyperlink.setFont(new Font(10));
        hyperlink.setTextFill(Color.web("#205A85")); // RAL 5010 //Color.web("#0076a3"));
        hyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String stringURL = "https://openweathermap.org";
                String osName = System.getProperty("os.name");
                if (osName.startsWith("Win")) {
                    try {
                        Desktop.getDesktop().browse(new URL(stringURL).toURI());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                } else { // (osName.startsWith("Linux") || osName.startsWith("Mac")) {
                    try {
                        new ProcessBuilder("x-www-browser", stringURL).start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        vBoxEnd.getChildren().addAll(hyperlink); // TODO ustawić hyperlink na stałe na samym dole widgetu
    }

    private void createGeneralContextMenu() {
        generalContextMenu = new ContextMenu();
        CheckMenuItem alwaysOnTop = new CheckMenuItem("Always on top");
        alwaysOnTop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) vBoxMain.getScene().getWindow();
                if(stage.isAlwaysOnTop()) {
                    stage.setAlwaysOnTop(false);
                    alwaysOnTop.setSelected(false);
                } else {
                    stage.setAlwaysOnTop(true);
                    alwaysOnTop.setSelected(true);
                }
            }
        });
        generalContextMenu.getItems().add(alwaysOnTop);

        Menu parentMenuSize = new Menu("Size");
        checkMenuItemChildBig = new CheckMenuItem("Big");
        checkMenuItemChildBig.setSelected(true);
        checkMenuItemChildSmall = new CheckMenuItem("Small");
        checkMenuItemChildBig.setOnAction(e -> {
            Stage stage = (Stage) vBoxMain.getScene().getWindow();
            stage.setHeight(MainStage.getHeightBigPrimaryStage());
            stage.setWidth(MainStage.getWidthBigPrimaryStage());
            checkMenuItemChildBig.setSelected(true);
            checkMenuItemChildSmall.setSelected(false);
        });
        checkMenuItemChildSmall.setOnAction(e -> {
            Stage stage = (Stage) vBoxMain.getScene().getWindow();
            stage.setHeight(MainStage.getHeightSmallPrimaryStage()); // 135
            stage.setWidth(MainStage.getWidthSmallPrimaryStage()); // 240
            checkMenuItemChildBig.setSelected(false);
            checkMenuItemChildSmall.setSelected(true);
        });
        parentMenuSize.getItems().addAll(checkMenuItemChildBig, checkMenuItemChildSmall);
        generalContextMenu.getItems().add(parentMenuSize);

        createOpacityCheckMenuItems();

        SeparatorMenuItem separatorMenuItem1 = new SeparatorMenuItem();
        generalContextMenu.getItems().add(separatorMenuItem1);

        MenuItem settings = new MenuItem("Settings");
        settings.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                createStage("/fxml/Settings.fxml", "Settings");
                MainStage.mainController.setVBoxMainDisable();
            }
        });
        generalContextMenu.getItems().add(settings);
        SeparatorMenuItem separatorMenuItem2 = new SeparatorMenuItem();
        generalContextMenu.getItems().add(separatorMenuItem2);

        MenuItem aboutAuthor = new MenuItem("About author");
        aboutAuthor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                createStage("/fxml/AboutAuthor.fxml", "Author: Łukasz Kępka 2019");
            }
        });
        generalContextMenu.getItems().add(aboutAuthor);

        SeparatorMenuItem separatorMenuItem3 = new SeparatorMenuItem();
        generalContextMenu.getItems().add(separatorMenuItem3);
        MenuItem aboutProgram = new MenuItem("About program");
        aboutProgram.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                createStage("/fxml/AboutProgram.fxml", "About program");
            }
        });
        generalContextMenu.getItems().add(aboutProgram);
        SeparatorMenuItem separatorMenuItem4 = new SeparatorMenuItem();
        generalContextMenu.getItems().add(separatorMenuItem4);
        MenuItem exit = new MenuItem("Exit program");
        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                closeApplication(actionEvent);
            }
        });
        generalContextMenu.getItems().add(exit);
        vBoxMain.setOnContextMenuRequested(e -> {
            generalContextMenu.show(vBoxMain.getScene().getWindow(), e.getScreenX(), e.getScreenY());
        });
    }

    private void closeApplication(ActionEvent actionEvent) {
        MainStage.saveToFilePosition("./src/main/resources/txt/widgetPosition.txt");
        Platform.exit();
        System.exit(0);
    }

//    public void saveToFilePosition(String path_file) {
//        File myFile = new File(path_file);
//        myFile.delete();
//        if(!myFile.exists()) {
//            try {
//                FileWriter fileWriter = new FileWriter(path_file);
//                fileWriter.write("positionX/" + MainStage.getPrimaryStage().getX() + "\n");
//                fileWriter.write("positionY/" + MainStage.getPrimaryStage().getY() + "\n");
//                fileWriter.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    private void createStage(String resourceFxml, String stageTitle) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource(resourceFxml));
        Pane pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(pane);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(stageTitle);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        stage.toFront();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                MainStage.mainController.setVBoxMainNotDisable();
            }
        });

        pane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                positionX = stage.getX() - event.getScreenX();
                positionY = stage.getY() - event.getScreenY();
            }
        });

        pane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() + positionX);
                stage.setY(event.getScreenY() + positionY);
            }
        });
    }

    private void createOpacityCheckMenuItems() {
        opacityMenu = new Menu("Opacity");
        String [] opacityPercent = {"5","10","20","40","60","80","100"};
        double [] opacityValue = {0.05,0.10,0.20,0.40,0.60,0.80,1.00};
        opacityCheckMenuItem = new CheckMenuItem[opacityPercent.length];
        for(int i=0; i < opacityCheckMenuItem.length; i++) {
            int k = i;
            opacityCheckMenuItem[i] = new CheckMenuItem(opacityPercent[i]+"%");
            opacityCheckMenuItem[i].setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    setOpacity(opacityValue[k], k);
                }
            });
        }
        opacityCheckMenuItem[opacityCheckMenuItem.length - 1].setSelected(true);
        opacityMenu.getItems().addAll(opacityCheckMenuItem);
        generalContextMenu.getItems().add(opacityMenu);
    }

    private void setOpacity(double opacityValue, int i) {
        Stage stage = (Stage) vBoxMain.getScene().getWindow();
        stage.setOpacity(opacityValue);

        for(int j=0; j < opacityCheckMenuItem.length; j++) {
            opacityCheckMenuItem[j].setSelected(false);
        }
        opacityCheckMenuItem[i].setSelected(true);
    }

    public void setVBoxMainNotDisable() {
        vBoxMain.setDisable(false);
    }
    public void setVBoxMainDisable() {
        vBoxMain.setDisable(true);
    }
    public Label getLabelDescription() {
        return labelDescription;
    }
    public Label getLabelHumidity() {
        return labelHumidity;
    }
    public Label getLabelSunrise() {
        return labelSunrise;
    }
    public Label getLabelSunset() {
        return labelSunset;
    }
    public Label getLabelLastWeatherUpdate() {
        return labelLastWeatherUpdate;
    }

    public static boolean isInternetConnection() {
        runSystemCommand("ping openweathermap.org");
        return isInternetConnection;
    }

    public void delaySeconds(int timeToDelay) { // TOOO delete this function, only temporary
        timeToDelay = timeToDelay * 1000;
        System.out.println(("(#MainController) start delay = " + timeToDelay));
        try {
            Thread.sleep(timeToDelay); // 10 min -> 600_000
        } catch (InterruptedException e) {
            System.out.println(" Funkcja delay() \n");
            e.printStackTrace();
        }
        System.out.println(("(#MainController) end delay = " + timeToDelay));
    }

    public static void runSystemCommand(String command) {
        Process p;
        String str = "";
        try {
            p = Runtime.getRuntime().exec(command);
            BufferedReader inputStream = new BufferedReader( new InputStreamReader(p.getInputStream()));
            while((str = inputStream.readLine()) != null) {
                System.out.println("(#MainController) runSystemCommand(), str = " + str);
                isInternetConnection = true;
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        isInternetConnection = false;
        System.out.println("(#MainController) runSystemCommand(), str = " + str + " | isInternetConnection = " + isInternetConnection);
    }
}
