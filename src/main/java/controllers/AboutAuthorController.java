package controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AboutAuthorController {

    @FXML
    private VBox vBoxAboutAuthor;

    @FXML
    public void initialize() {
        createContextMenu();
    }

    private void createContextMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem closeWindow = new MenuItem("Close window");
        closeWindow.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) vBoxAboutAuthor.getScene().getWindow();
                stage.close();
            }
        });
        contextMenu.getItems().add(closeWindow);
        vBoxAboutAuthor.setOnContextMenuRequested(e -> {
            contextMenu.show(vBoxAboutAuthor.getScene().getWindow(), e.getScreenX(), e.getScreenY());
        });
    }

}
