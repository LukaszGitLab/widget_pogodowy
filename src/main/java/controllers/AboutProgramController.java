package controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class AboutProgramController {

    @FXML
    private VBox vBoxAboutProgram;
    @FXML
    private Hyperlink hyperlinkAboutProgram;

    @FXML
    public void initialize() {
        createContextMenu();
    }

    private void createContextMenu() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem closeWindow = new MenuItem("Close window");
        closeWindow.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) vBoxAboutProgram.getScene().getWindow();
                stage.close();
            }
        });
        contextMenu.getItems().add(closeWindow);
        vBoxAboutProgram.setOnContextMenuRequested(e -> {
            contextMenu.show(vBoxAboutProgram.getScene().getWindow(), e.getScreenX(), e.getScreenY());
        });
    }

    public void onActionAboutProgram(ActionEvent actionEvent) {
        String stringURL = "https://openweathermap.org";
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Win")) {
            try {
                Desktop.getDesktop().browse(new URL(stringURL).toURI());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else { // (osName.startsWith("Linux") || osName.startsWith("Mac")) {
            try {
                new ProcessBuilder("x-www-browser", stringURL).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
