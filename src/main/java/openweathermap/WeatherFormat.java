package openweathermap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import controllers.MainController;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WeatherFormat {
    private final String PATH_FILE = "./src/main/resources/txt/widgetSettings.txt";
    private com.fasterxml.jackson.databind.ObjectMapper mapper;
    public String SERVER_ADDRESS;
    private Map<String,String> mapForFile = new HashMap<String,String>();

    public WeatherFormat() {
        mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        configureUnirest();
    }

    public WeatherFormatDTO getAll() throws UnirestException, IOException {
        readSettingsFromFile();
        WeatherFormatDTO weatherFromApi = Unirest.get(SERVER_ADDRESS)
                .asObject(WeatherFormatDTO.class).getBody();
        System.out.println("(#WeatherFormat) weatherFromApi = " + weatherFromApi);
        mapper.writeValue(new File("data/weather.json"),weatherFromApi);
        return weatherFromApi;
    }

    private void readSettingsFromFile() {
        try {
            FileReader myFileReader = new FileReader(PATH_FILE);
            BufferedReader myBufferedReader = new BufferedReader(myFileReader);
            String textFromFile = "";

            while((textFromFile = myBufferedReader.readLine()) != null) {
                String[] stringTab = textFromFile.split("/");
                mapForFile.put(stringTab[0], stringTab[1]);
            }
        } catch (FileNotFoundException e) {
            new MainController().setVBoxMainNotDisable();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("(#WeatherFormat) " + "mapForFile" + mapForFile);
        SERVER_ADDRESS = "http://api.openweathermap.org/data/2.5/weather?" +
        mapForFile.get("temperatureUnit") + "&" +
        mapForFile.get("cityIdOrGeoLocation") + "&" +
        mapForFile.get("lang") + "&" +
        mapForFile.get("apiKey");
        System.out.println("(#WeatherFormat) SERVER_ADDRESS = " + SERVER_ADDRESS);

    }

    public void configureUnirest() {
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    System.out.println("(#WeatherFormat) configureUnirest() readValue RuntimeException");
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    System.out.println("(#WeatherFormat) configureUnirest() writeValue RuntimeException");
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
