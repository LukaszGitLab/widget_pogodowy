package main;

import controllers.MainController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class MainStage extends Application {

	public static MainController mainController;
	private static Stage pStage;
	private double positionX = 160.0;
	private double positionY = 200.0;
	private static double heightBigPrimaryStage = 350.0;
	private static double widthBigPrimaryStage = 270.0;
	private static double heightSmallPrimaryStage = 135.0;
	private static double widthSmallPrimaryStage = 240.0;
	private static double opacityPrimaryStage = 1.0;

	private Map<String,String> mapForFilePosition = new HashMap<String,String>();

	private final String PATH_FILE_POSITION = "./src/main/resources/txt/widgetPosition.txt";

	public static void main(String[] args)
	{
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("/fxml/MainWeather.fxml"));

		File myFile = new File(PATH_FILE_POSITION);
		if(!myFile.exists()) {
//			try {
//				FileWriter fileWriter = new FileWriter(PATH_FILE_POSITION);
//				fileWriter.close();
//			}  catch (IOException e) {
//				e.printStackTrace();
//			}
			String osName = System.getProperty("os.name");
			if (osName.startsWith("Win")) {
				heightBigPrimaryStage = 350;
				widthBigPrimaryStage = 264;
				heightSmallPrimaryStage = 135;
				widthSmallPrimaryStage = 240;
			} else {
				heightBigPrimaryStage = 350;
				widthBigPrimaryStage = 270;
				heightSmallPrimaryStage = 135;
				widthSmallPrimaryStage = 240;
			}
			try {
				FileWriter fileWriter = new FileWriter(PATH_FILE_POSITION);
				fileWriter.write("positionX/" + positionX + "\n");
				fileWriter.write("positionY/" + positionY + "\n");
				fileWriter.write("widthPrimaryStage/" + widthBigPrimaryStage + "\n");
				fileWriter.write("heightPrimaryStage/" + heightBigPrimaryStage + "\n");
//				fileWriter.write("widthSmallPrimaryStage/" + getPrimaryStage().getWidth() + "\n");
//				fileWriter.write("heightSmallPrimaryStage/" + getPrimaryStage().getHeight() + "\n");
				fileWriter.write("opacityPrimaryStage/" + opacityPrimaryStage + "\n");
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		VBox vBox = loader.load();
//		File
//				myFile = new File(PATH_FILE_POSITION);
//		if(myFile.exists()) {
//			String osName = System.getProperty("os.name");
//			if (osName.startsWith("Win")) {
//				heightBigPrimaryStage = 350;
//				widthBigPrimaryStage = 264;
//				heightSmallPrimaryStage = 135;
//				widthSmallPrimaryStage = 240;
//			} else {
//				heightBigPrimaryStage = 350;
//				widthBigPrimaryStage = 270;
//				heightSmallPrimaryStage = 135;
//				widthSmallPrimaryStage = 240;
//			}
//			try {
//				FileWriter fileWriter = new FileWriter(PATH_FILE_POSITION);
//				fileWriter.write("positionX/" + positionX + "\n");
//				fileWriter.write("positionY/" + positionY + "\n");
//				fileWriter.write("widthPrimaryStage/" + widthBigPrimaryStage + "\n");
//				fileWriter.write("heightPrimaryStage/" + heightBigPrimaryStage + "\n");
////				fileWriter.write("widthSmallPrimaryStage/" + getPrimaryStage().getWidth() + "\n");
////				fileWriter.write("heightSmallPrimaryStage/" + getPrimaryStage().getHeight() + "\n");
//				fileWriter.write("opacityPrimaryStage/" + opacityPrimaryStage + "\n");
//				fileWriter.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}

		mainController = (MainController) loader.getController();

		Scene scene = new Scene(vBox);

		setPrimaryStage(primaryStage);
		pStage = primaryStage;

		primaryStage.setScene(scene);
		primaryStage.setTitle("Łukasz Kępka, Pogoda 2019");
//		primaryStage.setResizable(false);
//		File
				myFile = new File(PATH_FILE_POSITION);
//		String osName = System.getProperty("os.name");
//		if (osName.startsWith("Win")) {
//			heightBigPrimaryStage = 350;
//			widthBigPrimaryStage = 264;
//			heightSmallPrimaryStage = 135;
//			widthSmallPrimaryStage = 240;
//		} else {
//			heightBigPrimaryStage = 350;
//			widthBigPrimaryStage = 270;
//			heightSmallPrimaryStage = 135;
//			widthSmallPrimaryStage = 240;
//		}

		if(myFile.exists()) {
			copyDataFromFileTo(mapForFilePosition, PATH_FILE_POSITION);
			positionX = Double.parseDouble(mapForFilePosition.get("positionX"));
			positionY = Double.parseDouble(mapForFilePosition.get("positionY"));
//			widthBigPrimaryStage = Double.parseDouble(mapForFilePosition.get("widthBigPrimaryStage"));
//			heightBigPrimaryStage = Double.parseDouble(mapForFilePosition.get("heightBigPrimaryStage"));
			opacityPrimaryStage = Double.parseDouble(mapForFilePosition.get("opacityPrimaryStage"));
		}

		primaryStage.setX(positionX);
		primaryStage.setY(positionY);
		if(myFile.exists()) {
			primaryStage.setHeight(Double.parseDouble(mapForFilePosition.get("heightPrimaryStage"))); // 310
			primaryStage.setWidth(Double.parseDouble(mapForFilePosition.get("widthPrimaryStage")));
		}
//		else {
//			primaryStage.setHeight(getHeightBigPrimaryStage()); // 310
//			primaryStage.setWidth(getWidthBigPrimaryStage());
//		}

		primaryStage.setOpacity(opacityPrimaryStage);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.show();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent windowEvent) {
				saveToFilePosition(PATH_FILE_POSITION);
				Platform.exit();
				System.exit(0);
			}
		});

		vBox.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				positionX = primaryStage.getX() - event.getScreenX();
				positionY = primaryStage.getY() - event.getScreenY();
			}
		});

		vBox.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				primaryStage.setX(event.getScreenX() + positionX);
				primaryStage.setY(event.getScreenY() + positionY);
			}
		});
	}

	public static void saveToFilePosition(String path_file) {
		System.out.println("\n\n(#MainStage) getPrimaryStage().getWidth() = " + getPrimaryStage().getWidth() + "\n\n");

		File myFile = new File(path_file);
		myFile.delete();
		if(!myFile.exists()) {
			try {
				FileWriter fileWriter = new FileWriter(path_file);
				fileWriter.write("positionX/" + getPrimaryStage().getX() + "\n");
				fileWriter.write("positionY/" + getPrimaryStage().getY() + "\n");
				fileWriter.write("widthPrimaryStage/" + getPrimaryStage().getWidth() + "\n");
				fileWriter.write("heightPrimaryStage/" + getPrimaryStage().getHeight() + "\n");
//				fileWriter.write("widthSmallPrimaryStage/" + getPrimaryStage().getWidth() + "\n");
//				fileWriter.write("heightSmallPrimaryStage/" + getPrimaryStage().getHeight() + "\n");
				fileWriter.write("opacityPrimaryStage/" + getPrimaryStage().getOpacity() + "\n");
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void copyDataFromFileTo(Map map, String pathFile) {
		try {
			FileReader myFileReader = new FileReader(pathFile);
			BufferedReader myBufferedReader = new BufferedReader(myFileReader);
			String textFromFile = "";

			while((textFromFile = myBufferedReader.readLine()) != null) {
				String[] stringTab = textFromFile.split("/");
				map.put(stringTab[0], stringTab[1]);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Stage getPrimaryStage() {
		return pStage;
	}

	public static void setPrimaryStage(Stage pStage) {
		MainStage.pStage = pStage;
	}

	public static double getHeightBigPrimaryStage() {
		return heightBigPrimaryStage;
	}

	public static void setHeightBigPrimaryStage(double heightBigPrimaryStage) {
		MainStage.heightBigPrimaryStage = heightBigPrimaryStage;
	}

	public static double getWidthBigPrimaryStage() {
		return widthBigPrimaryStage;
	}

	public static void setWidthBigPrimaryStage(double widthBigPrimaryStage) {
		MainStage.widthBigPrimaryStage = widthBigPrimaryStage;
	}

	public static double getHeightSmallPrimaryStage() {
		return heightSmallPrimaryStage;
	}

	public static void setHeightSmallPrimaryStage(double heightSmallPrimaryStage) {
		MainStage.heightSmallPrimaryStage = heightSmallPrimaryStage;
	}

	public static double getWidthSmallPrimaryStage() {
		return widthSmallPrimaryStage;
	}

	public static void setWidthSmallPrimaryStage(double widthSmallPrimaryStage) {
		MainStage.widthSmallPrimaryStage = widthSmallPrimaryStage;
	}

	public static double getOpacityPrimaryStage() {
		return opacityPrimaryStage;
	}

	public static void setOpacityPrimaryStage(double opacityPrimaryStage) {
		MainStage.opacityPrimaryStage = opacityPrimaryStage;
	}
}
