package openweathermap5day3hourforecast;

import java.util.Arrays;

public class WeatherFormat5Day3HourDTO {
    private String cod;
    private Double message;
    private Double cnt;
    private Lista[] list;
    private City city;

    public WeatherFormat5Day3HourDTO() {
    }

    public WeatherFormat5Day3HourDTO(String cod, Double message, Double cnt, Lista[] list, City city) {
        this.cod = cod;
        this.message = message;
        this.cnt = cnt;
        this.list = list;
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Double getCnt() {
        return cnt;
    }

    public void setCnt(Double cnt) {
        this.cnt = cnt;
    }

    public Lista[] getList() {
        return list;
    }

    public void setList(Lista[] list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "WeatherFormat5Day3HourDTO{" +
                "cod='" + cod + '\'' +
                ", message=" + message +
                ", cnt=" + cnt +
                ", list=" + Arrays.toString(list) +
                ", city=" + city +
                '}';
    }
}
