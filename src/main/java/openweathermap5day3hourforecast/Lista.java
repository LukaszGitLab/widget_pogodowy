package openweathermap5day3hourforecast;

import openweathermap.Clouds;
import openweathermap.Weather;
import openweathermap.Wind;

import java.util.Arrays;
import java.util.Map;

public class Lista {
    private int dt;
    private Main main;
    private Weather[] weather;
    private Clouds clouds;
    private Wind wind;
    private Map<String, Integer> rain;
    private Map<String, Integer> snow;
    private Sys sys;
    private String dt_txt;
    private int visibility;
    private int pop;

    public Lista() {
    }

    public Lista(int dt, Main main, Weather[] weather, Clouds clouds, Wind wind, Map<String, Integer> rain, Map<String, Integer> snow, Sys sys, String dt_txt, int visibility, int pop) {
        this.dt = dt;
        this.main = main;
        this.weather = weather;
        this.clouds = clouds;
        this.wind = wind;
        this.rain = rain;
        this.snow = snow;
        this.sys = sys;
        this.dt_txt = dt_txt;
        this.visibility = visibility;
        this.pop = pop;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Map<String, Integer> getRain() {
        return rain;
    }

    public void setRain(Map<String, Integer> rain) {
        this.rain = rain;
    }

    public Map<String, Integer> getSnow() {
        return snow;
    }

    public void setSnow(Map<String, Integer> snow) {
        this.snow = snow;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int getPop() {
        return pop;
    }

    public void setPop(int pop) {
        this.pop = pop;
    }

    @Override
    public String toString() {
        return "Lista{" +
                "dt=" + dt +
                ", main=" + main +
                ", weather=" + Arrays.toString(weather) +
                ", clouds=" + clouds +
                ", wind=" + wind +
                ", rain=" + rain +
                ", snow=" + snow +
                ", sys=" + sys +
                ", dt_txt='" + dt_txt + '\'' +
                ", visibility=" + visibility +
                ", pop=" + pop +
                '}';
    }
}
