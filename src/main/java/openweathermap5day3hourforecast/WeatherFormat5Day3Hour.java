package openweathermap5day3hourforecast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import controllers.MainController;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WeatherFormat5Day3Hour {
    private final String PATH_FILE = "./src/main/resources/txt/widgetSettings.txt";
    public com.fasterxml.jackson.databind.ObjectMapper mapper;
    public String SERVER_ADDRESS;
    private Map<String,String> mapForFile = new HashMap<String,String>();

    public WeatherFormat5Day3Hour() {
        mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        configureUnirest();
    }

    public WeatherFormat5Day3HourDTO getAll() throws UnirestException, IOException {
        readSettingsFromFile();
        WeatherFormat5Day3HourDTO weatherFromApi5Day3Hour = Unirest.get(SERVER_ADDRESS)
                .asObject(WeatherFormat5Day3HourDTO.class).getBody();
        mapper.writeValue(new File("data/weather5Day3Hour.json"),weatherFromApi5Day3Hour);
        return weatherFromApi5Day3Hour;
    }

    private void readSettingsFromFile() {
        try {
            FileReader myFileReader = new FileReader(PATH_FILE);
            BufferedReader myBufferedReader = new BufferedReader(myFileReader);
            String textFromFile = "";

            while((textFromFile = myBufferedReader.readLine()) != null) {
                String[] stringTab = textFromFile.split("/");
                mapForFile.put(stringTab[0], stringTab[1]);
            }
        } catch (FileNotFoundException e) {
            new MainController().setVBoxMainNotDisable();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        SERVER_ADDRESS = "http://api.openweathermap.org/data/2.5/forecast?&mode=json&" +
                mapForFile.get("temperatureUnit") + "&" +
                mapForFile.get("cityIdOrGeoLocation") + "&" +
                mapForFile.get("lang") + "&" +
                mapForFile.get("apiKey");
        System.out.println("(#WeatherFormat5Days3Hour) SERVER_ADDRESS = " + SERVER_ADDRESS);
    }

    public void configureUnirest(){
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    System.out.println("(#WeatherFormat5Days3Hour) configureUnirest() readValue IOException");
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    System.out.println("(#WeatherFormat5Days3Hour) configureUnirest() writeValue IOException");
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
