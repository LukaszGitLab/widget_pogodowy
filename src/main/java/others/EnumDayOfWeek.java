package others;

public enum EnumDayOfWeek {
    MON(1), TUE(2), WED(3), THU(4), FRI(5), SAT(6), SUN(7);

    private final int value;

    EnumDayOfWeek(final int value) {
        this.value = value;
    }

    public EnumDayOfWeek getValue(int value) {
        for(EnumDayOfWeek e: EnumDayOfWeek.values()) {
            if(e.value == value) {
                return e;
            }
        }
        return null;// not found
    }
}
