package others;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import java.awt.*;

public class MyClassTextFieldListener implements ChangeListener<String> {
//    private String myRegex = "-?((\\d*)|(\\d+(\\.|,)\\d*))";
    private String regex;
    private TextField textField;

    public MyClassTextFieldListener(TextField textField, String regex) {
        this.textField = textField;
        this.regex = regex;
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if(newValue.matches(regex)) {
            textField.setText(newValue);
        } else {
            textField.setText(oldValue);
            Toolkit.getDefaultToolkit().beep();
        }
    }
}
